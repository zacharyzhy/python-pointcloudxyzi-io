#cython: embedsignature=True
from collections import Sequence
import numbers
import numpy as np
# importing numpy c++ support
cimport numpy as cnp

cimport pcl_defs as cpp
cimport cython
from cython.operator import dereference as deref
from libcpp.string cimport string
from libcpp cimport bool
from libcpp.vector cimport vector

cnp.import_array()

cdef class PointCloudXYZ:
    """Represents a cloud of points in 3-d space.

    A point cloud can be initialized from either a NumPy ndarray of shape
    (n_points, 3), from a list of triples, or from an integer n to create an
    "empty" cloud of n points.

    To load a point cloud from disk, use pcl.load.
    """
    cdef cpp.PointCloud[cpp.PointXYZ] *thisptr

    def __cinit__(self, init=None):
        self.thisptr = new cpp.PointCloud[cpp.PointXYZ]()

        if init is None:
            return
        elif isinstance(init, (numbers.Integral, np.integer)):
            self.resize(init)
        elif isinstance(init, np.ndarray):
            self.from_array(init)
        elif isinstance(init, Sequence):
            self.from_list(init)
        else:
            raise TypeError("Can't initialize a PointCloud from a %s"
                            % type(init))

    def __dealloc__(self):
        del self.thisptr
    property width:
        """ property containing the width of the point cloud """
        def __get__(self): return self.thisptr.width
    property height:
        """ property containing the height of the point cloud """
        def __get__(self): return self.thisptr.height
    property size:
        """ property containing the number of points in the point cloud """
        def __get__(self): return self.thisptr.size()
    property is_dense:
        """ property containing whether the cloud is dense or not """
        def __get__(self): return self.thisptr.is_dense

    def __repr__(self):
        return "<PointCloud of %d points>" % self.size

    @cython.boundscheck(False)
    def from_array(self, cnp.ndarray[cnp.float32_t, ndim=2] arr not None):
        """
        Fill this object from a 2D numpy array (float32)
        """
        assert arr.shape[1] == 3

        cdef cnp.npy_intp npts = arr.shape[0]
        self.resize(npts)
        self.thisptr.width = npts
        self.thisptr.height = 1

        cdef cpp.PointXYZ *p
        for i in range(npts):
            p = cpp.getptr(self.thisptr, i)
            p.x, p.y, p.z = arr[i, 0], arr[i, 1], arr[i, 2]

    @cython.boundscheck(False)
    def to_array(self):
        """
        Return this object as a 2D numpy array (float32)
        """
        cdef float x,y,z
        cdef cnp.npy_intp n = self.thisptr.size()
        cdef cnp.ndarray[cnp.float32_t, ndim=2, mode="c"] result
        cdef cpp.PointXYZ *p

        result = np.empty((n, 3), dtype=np.float32)

        for i in range(n):
            p = cpp.getptr(self.thisptr, i)
            result[i, 0] = p.x
            result[i, 1] = p.y
            result[i, 2] = p.z
        return result

    def from_list(self, _list):
        """
        Fill this pointcloud from a list of 3-tuples
        """
        cdef Py_ssize_t npts = len(_list)
        cdef cpp.PointXYZ *p

        self.resize(npts)
        self.thisptr.width = npts
        self.thisptr.height = 1
        for i, l in enumerate(_list):
            p = cpp.getptr(self.thisptr, i)
            p.x, p.y, p.z = l

    def to_list(self):
        """
        Return this object as a list of 3-tuples
        """
        return self.to_array().tolist()

    def resize(self, cnp.npy_intp x):
        self.thisptr.resize(x)

    def get_point(self, cnp.npy_intp row, cnp.npy_intp col):
        """
        Return a point (3-tuple) at the given row/column
        """
        cdef cpp.PointXYZ *p = cpp.getptr_at(self.thisptr, row, col)
        return p.x, p.y, p.z

    def __getitem__(self, cnp.npy_intp idx):
        cdef cpp.PointXYZ *p = cpp.getptr_at(self.thisptr, idx)
        return p.x, p.y, p.z

    def from_file(self, char *f):
        """
        Fill this pointcloud from a file (a local path).
        Only pcd files supported currently.

        Deprecated; use pcl.load instead.
        """
        return self._from_pcd_file(f)

    def _from_pcd_file(self, const char *s):
        cdef int error = 0
        with nogil:
            ok = cpp.loadPCDFile(string(s), deref(self.thisptr))
        return error

    def _from_ply_file(self, const char *s):
        cdef int ok = 0
        with nogil:
            error = cpp.loadPLYFile(string(s), deref(self.thisptr))
        return error

    def to_file(self, const char *fname, bool ascii=True):
        """Save pointcloud to a file in PCD format.

        Deprecated: use pcl.save instead.
        """
        return self._to_pcd_file(fname, not ascii)

    def _to_pcd_file(self, const char *f, bool binary=False):
        cdef int error = 0
        cdef string s = string(f)
        with nogil:
            error = cpp.savePCDFile(s, deref(self.thisptr), binary)
        return error

    def _to_ply_file(self, const char *f, bool binary=False):
        cdef int error = 0
        cdef string s = string(f)
        with nogil:
            error = cpp.savePLYFile(s, deref(self.thisptr), binary)
        return error



cdef class PointCloudXYZI:
    """Represents a cloud of points in 3-d space.

    A point cloud can be initialized from either a NumPy ndarray of shape
    (n_points, 4), from a list of quadruples, or from an integer n to create an
    "empty" cloud of n points.

    To load a point cloud from disk, use pcl.load.
    """
    cdef cpp.PointCloud[cpp.PointXYZI] *thisptr

    def __cinit__(self, init=None):
        self.thisptr = new cpp.PointCloud[cpp.PointXYZI]()

        if init is None:
            return
        elif isinstance(init, (numbers.Integral, np.integer)):
            self.resize(init)
        elif isinstance(init, np.ndarray):
            self.from_array(init)
        elif isinstance(init, Sequence):
            self.from_list(init)
        else:
            raise TypeError("Can't initialize a PointCloud from a %s"
                            % type(init))

    def __dealloc__(self):
        del self.thisptr
    property width:
        """ property containing the width of the point cloud """
        def __get__(self): return self.thisptr.width
    property height:
        """ property containing the height of the point cloud """
        def __get__(self): return self.thisptr.height
    property size:
        """ property containing the number of points in the point cloud """
        def __get__(self): return self.thisptr.size()
    property is_dense:
        """ property containing whether the cloud is dense or not """
        def __get__(self): return self.thisptr.is_dense

    def __repr__(self):
        return "<PointCloud of %d points>" % self.size

    @cython.boundscheck(False)
    def from_array(self, cnp.ndarray[cnp.float32_t, ndim=2] arr not None):
        """
        Fill this object from a 2D numpy array (float32)
        """
        assert arr.shape[1] == 4

        cdef cnp.npy_intp npts = arr.shape[0]
        self.resize(npts)
        self.thisptr.width = npts
        self.thisptr.height = 1

        cdef cpp.PointXYZI *p
        for i in range(npts):
            p = cpp.getptr2(self.thisptr, i)
            p.x, p.y, p.z, p.intensity = arr[i, 0], arr[i, 1], arr[i, 2], arr[i, 3]

    @cython.boundscheck(False)
    def to_array(self):
        """
        Return this object as a 2D numpy array (float32)
        """
        cdef float x,y,z,intensity
        cdef cnp.npy_intp n = self.thisptr.size()
        cdef cnp.ndarray[cnp.float32_t, ndim=2, mode="c"] result
        cdef cpp.PointXYZI *p

        result = np.empty((n, 4), dtype=np.float32)

        for i in range(n):
            p = cpp.getptr2(self.thisptr, i)
            result[i, 0] = p.x
            result[i, 1] = p.y
            result[i, 2] = p.z
            result[i, 3] = p.intensity
        return result

    def from_list(self, _list):
        """
        Fill this pointcloud from a list of 3-tuples
        """
        cdef Py_ssize_t npts = len(_list)
        cdef cpp.PointXYZI *p

        self.resize(npts)
        self.thisptr.width = npts
        self.thisptr.height = 1
        for i, l in enumerate(_list):
            p = cpp.getptr2(self.thisptr, i)
            p.x, p.y, p.z, p.intensity = l

    def to_list(self):
        """
        Return this object as a list of 3-tuples
        """
        return self.to_array().tolist()

    def resize(self, cnp.npy_intp x):
        self.thisptr.resize(x)

    def get_point(self, cnp.npy_intp row, cnp.npy_intp col):
        """
        Return a point (3-tuple) at the given row/column
        """
        cdef cpp.PointXYZI *p = cpp.getptr_at2(self.thisptr, row, col)
        return p.x, p.y, p.z, p.intensity

    def __getitem__(self, cnp.npy_intp idx):
        cdef cpp.PointXYZI *p = cpp.getptr_at2(self.thisptr, idx)
        return p.x, p.y, p.z, p.intensity

    def from_file(self, char *f):
        """
        Fill this pointcloud from a file (a local path).
        Only pcd files supported currently.

        Deprecated; use pcl.load instead.
        """
        return self._from_pcd_file(f)

    def _from_pcd_file(self, const char *s):
        cdef int error = 0
        with nogil:
            ok = cpp.loadPCDFile(string(s), deref(self.thisptr))
        return error

    def _from_ply_file(self, const char *s):
        cdef int ok = 0
        with nogil:
            error = cpp.loadPLYFile(string(s), deref(self.thisptr))
        return error

    def to_file(self, const char *fname, bool ascii=True):
        """Save pointcloud to a file in PCD format.

        Deprecated: use pcl.save instead.
        """
        return self._to_pcd_file(fname, not ascii)

    def _to_pcd_file(self, const char *f, bool binary=False):
        cdef int error = 0
        cdef string s = string(f)
        with nogil:
            error = cpp.savePCDFile(s, deref(self.thisptr), binary)
        return error

    def _to_ply_file(self, const char *f, bool binary=False):
        cdef int error = 0
        cdef string s = string(f)
        with nogil:
            error = cpp.savePLYFile(s, deref(self.thisptr), binary)
        return error
