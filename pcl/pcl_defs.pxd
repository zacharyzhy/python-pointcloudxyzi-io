from libc.stddef cimport size_t

from libcpp.vector cimport vector
from libcpp.string cimport string
from libcpp cimport bool
from shared_ptr cimport shared_ptr
from vector cimport vector as vector2

cdef extern from "pcl/point_cloud.h" namespace "pcl":
    cdef cppclass PointCloud[T]:
        PointCloud() except +
        PointCloud(unsigned int, unsigned int) except +
        unsigned int width
        unsigned int height
        bool is_dense
        void resize(size_t) except +
        size_t size()
        #T& operator[](size_t)
        #T& at(size_t) except +
        #T& at(int, int) except +
        shared_ptr[PointCloud[T]] makeShared()

cdef extern from "indexing.hpp":
    # Use these instead of operator[] or at.
    PointXYZ *getptr(PointCloud[PointXYZ] *, size_t)
    PointXYZ *getptr_at(PointCloud[PointXYZ] *, size_t) except +
    PointXYZ *getptr_at(PointCloud[PointXYZ] *, int, int) except +
    PointXYZI *getptr2(PointCloud[PointXYZI] *, size_t)
    PointXYZI *getptr_at2(PointCloud[PointXYZI] *, size_t) except +
    PointXYZI *getptr_at2(PointCloud[PointXYZI] *, int, int) except +

cdef extern from "pcl/point_types.h" namespace "pcl":
    cdef struct PointXYZ:
        PointXYZ()
        float x
        float y
        float z
    cdef struct PointXYZI:
        PointXYZI()
        float x
        float y
        float z
        float intensity

cdef extern from "Eigen/src/Core/util/Memory.h" namespace "Eigen":
    cdef cppclass aligned_allocator[T]:
        pass

# TODO: check where aligned_allocator_t is used 
ctypedef aligned_allocator[PointXYZ] aligned_allocator_t 
ctypedef vector2[PointXYZ, aligned_allocator_t] AlignedPointTVector_t

cdef extern from "pcl/PointIndices.h" namespace "pcl":
    #FIXME: I made this a cppclass so that it can be allocated using new (cython barfs otherwise), and
    #hence passed to shared_ptr. This is needed because if one passes memory allocated
    #using malloc (which is required if this is a struct) to shared_ptr it aborts with
    #std::bad_alloc
    #
    #I don't know if this is actually a problem. It is cpp and there is no incompatibility in
    #promoting struct to class in this instance
    cdef cppclass PointIndices:
        vector[int] indices

ctypedef PointIndices PointIndices_t
ctypedef shared_ptr[PointIndices] PointIndicesPtr_t

# TODO: is this simply way of adding support gonna work???
cdef extern from "pcl/io/pcd_io.h" namespace "pcl::io":
    int load(string file_name, PointCloud[PointXYZ] &cloud) nogil
    int loadPCDFile(string file_name, PointCloud[PointXYZ] &cloud) nogil
    int savePCDFile(string file_name, PointCloud[PointXYZ] &cloud,
                    bool binary_mode) nogil

    int load(string file_name, PointCloud[PointXYZI] &cloud) nogil
    int loadPCDFile(string file_name, PointCloud[PointXYZI] &cloud) nogil
    int savePCDFile(string file_name, PointCloud[PointXYZI] &cloud,
                    bool binary_mode) nogil

cdef extern from "pcl/io/ply_io.h" namespace "pcl::io":
    int loadPLYFile(string file_name, PointCloud[PointXYZ] &cloud) nogil
    int savePLYFile(string file_name, PointCloud[PointXYZ] &cloud,
                    bool binary_mode) nogil
    int loadPLYFile(string file_name, PointCloud[PointXYZI] &cloud) nogil
    int savePLYFile(string file_name, PointCloud[PointXYZI] &cloud,
                    bool binary_mode) nogil

# TODO: add PointXYZI
ctypedef PointCloud[PointXYZ] PointCloud_t
ctypedef shared_ptr[PointCloud[PointXYZ]] PointCloudPtr_t